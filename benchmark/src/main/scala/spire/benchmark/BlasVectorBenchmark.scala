package spire.benchmark

import spire.std.double._
import spire.syntax.vectorSpace._
import spire.algebra.VectorSpace
import spire.blas.Layout._
import spire.blas.{Vector, Matrix, DenseBuffer}
import spire.blas.DenseBuffer.ops._
import spire.blas.DenseBuffer.Buffer
import spire.blas.implicits._

import org.openjdk.jmh.annotations.{Benchmark, Scope, State}

import java.nio
import scala.language.postfixOps

@State(Scope.Thread)
class BlasVectorBenchmark {
  def mkBuff(n: Int) = nio.ByteBuffer.allocateDirect(n * 8).asDoubleBuffer
  type N = 1000
  val n = valueOf[N]
  val x = Vector[N].fromBuffer(
    {
      val b = mkBuff(n)
      for (i <- 0 until valueOf[N]) b.put(i, (i + 1).toDouble)
      b
    },
    colMajor
  )
  val one = Vector[N].fromBuffer(
    {
      val b = mkBuff(n)
      for (i <- 0 until valueOf[N]) b.put(i, 1.0)
      b
    },
    colMajor
  )
  val zero =
    VectorSpace[Matrix.Expr[Double, N, 1], Double].zero
  val output = mkBuff(n) //will be overwritten

  @Benchmark def add = {
    x + one unsafeToBuffer output
    output
  }

  @Benchmark def mult = {
    2.0 *: x unsafeToBuffer output
    output
  }

  @Benchmark def linLeft = {
    2.0 *: x + one unsafeToBuffer output
    output
  }

  @Benchmark def linRight = {
    x + one :* 2.0 unsafeToBuffer output
    output
  }

  /*@Benchmark def linab = {
    x :* 2.0 + one :* 3.0 unsafeToBuffer output //FIXME: thinks its field addition?
    output
  }*/

  @Benchmark def noop = {
    1.0 *: x + zero unsafeToBuffer output
    output
  }

  @Benchmark def many = {
    1.0 *: zero + 2.0 *: one + 3.0 *: x unsafeToBuffer output
    output
  }

}
