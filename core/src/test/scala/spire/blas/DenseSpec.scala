package spire.blas

import org.scalatest.{Matchers, FlatSpec}
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalacheck.{Arbitrary, Gen}

import spire.syntax.vectorSpace._
import spire.std.double._
import spire.blas.{Vector, Matrix}
import spire.blas.implicits._
import spire.blas.Layout._

import java.nio

class DenseSpec
    extends FlatSpec
    with Matchers
    with GeneratorDrivenPropertyChecks {

  implicit def arbEnum[E <: Enumeration: ValueOf]: Arbitrary[E#Value] =
    Arbitrary(Gen.oneOf(valueOf[E].values.toSeq))

  lazy val i = Array.fill(16)(1.0)
  lazy val o = Array.ofDim[Double](16)

  "dense.Vector" should "have reflexive from/unsafeToArray" in {
    forAll { (l1: Layout, s1: Option[Int], l2: Layout, s2: Option[Int]) =>
      Vector[16].fromArray(i, l1).unsafeToArray(o, l2) shouldEqual i
    }
  }

  it should "add mixed strides" in {
    //FIXME: will fail on big endian archs?
    val consecutive = nio.DoubleBuffer.allocate(6)
    val strided = nio.DoubleBuffer.allocate(9)
    val a = Vector[3].fromArray(Array[Double](1, 2, 3), colMajor) // Workaround for bug #5
    val zeros = Vector[3].fromBuffer(consecutive, colMajor);
    (a + zeros).toArray shouldEqual Array[Double](1, 2, 3) //stride 1
    (zeros + a).toArray shouldEqual Array[Double](1, 2, 3) //stride 1
    (a + zeros).unsafeToBuffer(strided, rowMajor, stride = Some(3))
    strided.array() shouldEqual Array[Double](1, 0, 0, 2, 0, 0, 3, 0, 0)
  }

  "dense.Matrix 4 x 4" should "have reflexive from/unsafeToArray" in {
    import spire.blas.implicits.optimizations._
    forAll { (l1: Layout, l2: Layout) =>
      Matrix[4, 4].fromArray(i, l1).unsafeToArray(o, l2) shouldEqual i
    }
  }

  "dense.Matrix 2 x 8" should "have reflexive from/unsafeToArray" in {
    import spire.blas.implicits.optimizations._
    forAll { (l1: Layout, l2: Layout) =>
      withClue("matrix 2 x 8") {
        Matrix[2, 8].fromArray(i, l1).unsafeToArray(o, l2) shouldEqual i
      }
      withClue("matrix 8 x 2") {
        Matrix[8, 2].fromArray(i, l1).unsafeToArray(o, l2) shouldEqual i
      }
    }
  }
}
