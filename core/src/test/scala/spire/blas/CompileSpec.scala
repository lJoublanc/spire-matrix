package spire.blas

import org.scalatest.{Matchers, FlatSpec}

import spire.algebra.Monoid
import DenseBuffer.ops._
import DenseBuffer.Buffer
import CBlas._
import cats.data.Writer
import cats.syntax.all._
import cats.effect.SyncIO
import cats.effect.concurrent.Ref

import scala.reflect.ClassTag
import scala.collection.immutable.ArraySeq
import scala.language.postfixOps
import java.nio.{ByteOrder, ByteBuffer}

/** This test checks that spire expressions are compiled down to the most efficient
  * BLAS routines.
  */
class CompileSpec extends FlatSpec with Matchers {
  import CompileSpec._
  import spire.syntax.vectorSpace._
  import spire.std.double._
  import spire.blas.{Matrix, Vector}
  import spire.blas.implicits._
  import spire.blas.implicits.optimizations._
  import spire.syntax.linearMap._

  implicit class CompareSyntax[T, B: DenseBuffer.Aux[T, ?]](x: Logger[B]) {
    def hasTrace(y: String) =
      x.get.unsafeRunSync().written shouldEqual y.stripMargin
  }

  "Pure API (A,B,C - Matrices | x,y - vectors | α,β - scalars)" should "reduce ax to COPY + SCAL" in {
    val x = Vector[1](2.0)
    val z = (3.0 *: x).toBuffer
    z hasTrace """copy(1,ArraySeq(2.0),1,ArraySeq(0.0),1)
                |scal(1,3.0,ArraySeq(2.0),1)"""
  }

  it should "reduce from ax + y to COPY + AXPY" in {
    val x = Vector[1](1.0)
    val y = Vector[1](2.0)
    (3.0 *: x + y toBuffer) hasTrace
      """copy(1,ArraySeq(2.0),1,ArraySeq(0.0),1)
          |axpy(1,3.0,ArraySeq(1.0),1,ArraySeq(2.0),1)"""
  }

  // format: off

  it should "reduce α A B + β C to COPY + GEMM" in {
    val a = ArraySeq(1.0, 2.0, 3.0,
                     4.0, 5.0, 6.0)
    val b = ArraySeq(1.0, 2.0, 3.0, 4.0,
                     5.0, 6.0, 7.0, 8.0,
                     9.0, 1.0, 2.0, 3.0)
    val c = ArraySeq(1.0, 1.0, 1.0, 1.0,
                     1.0, 1.0, 1.0, 1.0)
    val A = Matrix[2,3](a: _*)//this is to force a defensive copy.
    val B = Matrix[3,4](b: _*)
    val C = Matrix[2,4](c: _*)
    val α = 2.0
    val β = 3.0

    implicit val lm: spire.algebra.LinearMap[Matrix.Expr[Double,?,?]] = linearMap[Double,Logger[java.nio.DoubleBuffer]]
    implicit val lg: spire.algebra.LinearGroup[λ[m => Matrix.Expr[Double,m,m]]] = linearGroup[Double,Logger[java.nio.DoubleBuffer]]
    // FIXME: The above weren't necessary before we removed type parameter B from Matrix.Expr, and introduced 'Buffer[_] = Any`
    (α *: (A * B) + β *: C).toBuffer hasTrace
      s"""copy(8,$c,4,${ArraySeq.fill(8)(0.0)},4)
      |gemm(rowMajor,noTrans, noTrans, 2, 3, 3, $α, $a, 3, $b, 4, $β, $c, 4)"""
  }

  // format: on
}

object CompileSpec {

  private type Logger[B] = Ref[SyncIO, Writer[String, B]]

  private implicit val strMonoid: Monoid[String] = new Monoid[String] {
    def empty = ""
    def combine(s: String, t: String) =
      if (s.isEmpty) t
      else if (t.isEmpty) s
      else s + "\n" + t
  }

  /** A writer monad that traces the BLAS API calls made. */
  implicit def cblasWriter[F[_], K: ClassTag, B](
      implicit
      inner: CBlas.Aux[K, B],
      B: DenseBuffer.Aux[K, B]
  ): CBlas.Aux[K, Logger[B]] = new CBlas[K] {

    type Buffer = Logger[B]

    /** Make a defensive copy into an array */
    private def cp(b: B): ArraySeq[K] = {
      val n = B.size(b)
      val xs = Array.ofDim[K](n)
      for (i <- 0 until n) xs(i) = B.get(b)(i)
      ArraySeq.unsafeWrapArray(xs)
    }

    val copy = (size: Int, x: Logger[B], incx: Int, y: Logger[B], incy: Int) =>
      x.get
        .flatMap { x =>
          y.update { y =>
            (x product y) mapBoth {
              case (log, (x, y)) =>
                log.combine(s"copy($size,${cp(x)},$incx,${cp(y)},$incy)") -> {
                  inner.copy(size, x, incx, y, incy); y
                }
            }
          }
        }
        .unsafeRunSync()

    val scal = (size: Int, a: K, x: Logger[B], incx: Int) =>
      x.update { x =>
          x.mapBoth { (log, x) =>
            log.combine(s"scal($size,$a,${cp(x)},$incx)") -> {
              inner.scal(size, a, x, incx); x
            }
          }
        }
        .unsafeRunSync()

    val axpy =
      (size: Int, a: K, x: Logger[B], incx: Int, y: Logger[B], incy: Int) =>
        x.get
          .flatMap { x =>
            y.update { y =>
              (x product y) mapBoth {
                case (logs, (x, y)) =>
                  logs
                    .combine(s"axpy($size,$a,${cp(x)},$incx,${cp(y)},$incy)") -> {
                    inner.axpy(size, a, x, incx, y, incy); y
                  }
              }
            }
          }
          .unsafeRunSync()

    val gemm = (
        layout: Int,
        transa: Int,
        transb: Int,
        m: Int,
        n: Int,
        k: Int,
        α: K,
        A: Logger[B],
        lda: Int,
        B: Logger[B],
        ldb: Int,
        β: K,
        C: Logger[B],
        ldc: Int
    ) => {
      val io =
        A.get.flatMap { aWriter =>
          B.get.flatMap { bWriter =>
            C.update { cWriter =>
              (aWriter, bWriter, cWriter).tupled.mapBoth {
                case (logs, (a, b, c)) =>
                  (
                    logs |+| "gemm(" +
                      (
                        Layout(layout),
                        Transpose(transa),
                        Transpose(transb),
                        m,
                        n,
                        k,
                        α,
                        cp(a),
                        lda,
                        cp(b),
                        ldb,
                        β,
                        cp(c),
                        ldc
                      ).productIterator.to(Iterable).mkString(",") + ")", {
                      inner.gemm(
                        layout,
                        transa,
                        transb,
                        m,
                        n,
                        k,
                        α,
                        a,
                        lda,
                        b,
                        ldb,
                        β,
                        c,
                        ldc
                      )
                      c
                    }
                  )
              }
            }
          }
        }
      io.unsafeRunSync()
    }

    // format: off
    def symm: (Int, Int, Int, Int, Int, K, Buffer, Int, Buffer, Int, K, Buffer, Int) => Unit = ???

    def trmm: (Int, Int, Int, Int, Int, Int, Int, K, Buffer, Int, Buffer, Int) => Unit = ???

    def trsm: (Int, Int, Int, Int, Int, Int, Int, K, Buffer, Int, Buffer, Int) => Unit = ???
    // format: on

  }

  implicit def bufferWriter[T, B](
      implicit inner: DenseBuffer.Aux[T, B]
  ): DenseBuffer.Aux[T, Logger[B]] =
    new DenseBuffer[T] {
      type Nio = Logger[B]

      protected def elemSizeBytes: Int = ???

      protected def fromBytes(b: ByteBuffer): Logger[B] = ???

      def endianess = ByteOrder.LITTLE_ENDIAN

      override def allocateDirect(i: Int): Logger[B] =
        Ref.unsafe[SyncIO, Writer[String, B]] {
          assert(endianess == ByteOrder.LITTLE_ENDIAN)
          Writer value (inner toNio (inner allocateDirect i))
        }

      def slice(ref: Buffer[T])(start: Int, count: Int): Buffer[T] =
        inner.slice(toNio(ref).get.unsafeRunSync().value)(start, count)

      def get(ref: Buffer[T])(i: Int): T =
        inner.get(toNio(ref).get.unsafeRunSync().value)(i)

      def put(ref: Buffer[T])(i: Int, t: T): Buffer[T] =
        Ref.unsafe[SyncIO, Writer[String, B]] {
          toNio(ref) modify { writer =>
            (writer, writer map (b => inner.toNio(inner.put(b)(i, t))))
          } unsafeRunSync ()
        }

      def wrap(array: Array[T]): Logger[B] =
        Ref.unsafe[SyncIO, Writer[String, B]] {
          Writer value (inner toNio (inner wrap array))
        }

      def size(ref: Buffer[T]): Int =
        inner.size(
          toNio(ref).get
            .unsafeRunSync()
            .value
        )

      def toArray(ref: Buffer[T]): Array[T] =
        inner.toArray(toNio(ref).get.unsafeRunSync().value)

    }
}
