package spire.blas

import spire.algebra.{VectorSpace, Field, LinearGroup}
import spire.syntax.field._
import spire.blas.CBlas
import spire.blas.DenseBuffer.ops._
import spire.blas.DenseBuffer.Buffer
import UpLo._
import Layout._
import Diag._

import scala.reflect.ClassTag
//import scala.language.experimental.macros

/** @define BLAST [[http://www.netlib.org/blas/blast-forum/ BLAS Technical Forum]]
  */
trait Matrix[F[_, _, _]] {

  /** Apply function `f` element-wise. Endofunctor on `K`. */
  def map[
      K: Field,
      B: DenseBuffer.Aux[K, ?],
      M: ValueOf: ? <:< Int,
      N: ValueOf: ? <:< Int
  ](fk: F[K, M, N])(f: K => K): F[K, M, N]

  protected[blas] def coflatMap[
      K: Field,
      B: DenseBuffer.Aux[K, ?],
      M: ValueOf: ? <:< Int,
      N: ValueOf: ? <:< Int,
      P: ValueOf: ? <:< Int,
      Q: ValueOf: ? <:< Int
  ](fk: F[K, M, N])(ff: F[K, P, Q] => K, origin: (Int, Int)): F[K, M, N]

  def toArray[K, B, M, N](
      f: F[K, M, N]
  )(
      implicit
      K: Field[K],
      buff: DenseBuffer.Aux[K, B],
      M: ValueOf[M],
      N: ValueOf[N],
      MisInt: M <:< Int,
      NisInt: N <:< Int
  ): Array[K] =
    toBuffer(f).toArray

  /** @return The argument `a`. */
  def unsafeToArray[K, B, M, N](
      f: F[K, M, N],
      a: Array[K],
      layout: Layout = rowMajor,
      stride: Option[Int] = None
  )(
      implicit
      K: Field[K],
      buff: DenseBuffer.Aux[K, B],
      M: ValueOf[M],
      N: ValueOf[N],
      MisInt: M <:< Int,
      NisInt: N <:< Int
  ): Array[K] =
    unsafeToBuffer(f, buff wrap a toNio, layout, stride).toArray

  def toBuffer[
      K: Field,
      B: DenseBuffer.Aux[K, ?],
      M: ValueOf: ? <:< Int,
      N: ValueOf: ? <:< Int
  ](
      f: F[K, M, N]
  ): B =
    unsafeToBuffer(f, null.asInstanceOf[B])

  /** Evaluates the expression `f` and writes the result to `b`.
    * This method is side-effectful; it **overwrites** the content of `b`.
    *
    * Refer to $BLAST §2.2.1 for a thorough exposition of dense matrix storage.
    *
    * Note that for 1-d vectors, the `stride` parameter only corresponds to the
    * BLAS `inc` parameter when the layout is inverted. i.e. iff `m x 1 && layout
    * == rowMajor` then `lda == incx`.
    *
    * @param f A linear/matrix expression to be evaluted.
    * @param b A buffer to write to. If `null`, it behaves like [[toBuffer]].
    * @param layout The memory layout of `b`. i.e. whether the data represents
    *               consecutive rows or columns of the matrix.
    * @param stride The size of the leading dimension of `b`. If unspecified, the
    *               interpreter will pick a stride that avoids unecessary copies.
    * @return The argument `b` or a newly allocate buffer if `b` was `null`.
    * @see [[toBuffer]] for a pure alternative.
    */
  def unsafeToBuffer[
      K: Field,
      B: DenseBuffer.Aux[K, ?],
      M: ValueOf: ? <:< Int,
      N: ValueOf: ? <:< Int
  ](
      f: F[K, M, N],
      b: B,
      layout: Layout = rowMajor,
      stride: Option[Int] = None
  ): B

  def builder[M, N]: Builder[M, N]

  trait Builder[M, N] {
    def fromBuffer[K, B](
        a: Buffer[K],
        layout: Layout,
        stride: Option[Int] = None,
        uplo: Option[UpLo] = None,
        diag: Option[Diag] = None,
        packed: Boolean = false,
        banded: Boolean = false
    )(
        implicit
        K: Field[K],
        buff: DenseBuffer.Aux[K, B],
        cblas: CBlas.Aux[K, B],
        M: ValueOf[M],
        N: ValueOf[N],
        MToInt: M <:< Int,
        NToInt: N <:< Int
    ): F[K, M, N]

    /** @param stride The length of the leading dimension.
      *               If unspecified, it is inferred from the array. */
    def fromArray[K, B](
        a: Array[K],
        layout: Layout,
        stride: Option[Int] = None,
        uplo: Option[UpLo] = None,
        diag: Option[Diag] = None,
        packed: Boolean = false,
        banded: Boolean = false
    )(
        implicit
        K: Field[K],
        buff: DenseBuffer.Aux[K, B],
        cblas: CBlas.Aux[K, B],
        M: ValueOf[M],
        N: ValueOf[N],
        MToInt: M <:< Int,
        NToInt: N <:< Int
    ): F[K, M, N] =
      fromBuffer(
        buff wrap a,
        layout,
        stride,
        uplo,
        diag,
        packed,
        banded
      )

    /** General row-wise constructor. */
    def apply[K, B](x: K*)(
        implicit
        ct: ClassTag[K],
        K: Field[K],
        buff: DenseBuffer.Aux[K, B],
        cblas: CBlas.Aux[K, B],
        M: ValueOf[M],
        N: ValueOf[N],
        MToInt: M <:< Int,
        NToInt: N <:< Int
    ): F[K, M, N] =
      fromArray(x.toArray, rowMajor)

    /** Upper triangular matrix. */
    def upper[K, B](x: K*)(
        implicit ct: ClassTag[K],
        K: Field[K],
        buff: DenseBuffer.Aux[K, B],
        cblas: CBlas.Aux[K, B],
        M: ValueOf[M],
        N: ValueOf[N],
        MToInt: M <:< Int,
        NToInt: N <:< Int
    ): F[K, M, N] =
      fromArray(x.toArray, rowMajor, uplo = Some(UpLo.upper))

    /** Lower tirangular matrix. */
    def lower[K, B](x: K*)(
        implicit ct: ClassTag[K],
        K: Field[K],
        buff: DenseBuffer.Aux[K, B],
        cblas: CBlas.Aux[K, B],
        M: ValueOf[M],
        N: ValueOf[N],
        MToInt: M <:< Int,
        NToInt: N <:< Int
    ): F[K, M, N] =
      fromArray(x.toArray, rowMajor, uplo = Some(UpLo.lower))

    /** A banded matrix. That is, a matrix where the upper and lower diagonals are zeros. */
    def banded[K, B](x: K*)(
        implicit ct: ClassTag[K],
        K: Field[K],
        buff: DenseBuffer.Aux[K, B],
        cblas: CBlas.Aux[K, B],
        M: ValueOf[M],
        N: ValueOf[N],
        MToInt: M <:< Int,
        NToInt: N <:< Int
    ): F[K, M, N] =
      ???

    /** A matrix where each element `(i,j) == (j,i)`. */
    def symmetric[K, B](x: K*)(
        implicit ct: ClassTag[K],
        K: Field[K],
        buff: DenseBuffer.Aux[K, B],
        cblas: CBlas.Aux[K, B],
        M: ValueOf[M],
        N: ValueOf[N],
        MToInt: M <:< Int,
        NToInt: N <:< Int
    ): F[K, M, N] =
      ???

    /** A compext matrix where each `(i,j) == conjugate((i,j))`. */
    //def hermitian[K, B](x: K*)(implicit cblas: CBlas.Aux[K,B], M: ValueOf[M], N: ValueOf[N]): F[M,N,K,B]

    /** Matrix with all elements initialized to `x`. */
    //def fill[M: ? <:< Int: ValueOf, N: ? <:< Int: ValueOf](x: Scalar): F[M,N]

    /** Matrix with all elements initialized to zero. */
    def zero[K](
        implicit
        buff: DenseBuffer.Aux[K, B] forSome { type B },
        G: VectorSpace[F[K, M, N], K]
    ): F[K, M, N] =
      G.zero

    def id[K](
        implicit isSquare: M =:= N,
        G: LinearGroup[λ[m => F[K, m, m]]],
        M: ValueOf[M],
        MisInt: M <:< Int
    ): F[K, M, M] =
      G.id
  }
}

object Matrix {

  /** Continuation used to represent a matrix expression. */
  type Expr[K, M, N] = (Buffer[K], Layout, Option[Int]) => Buffer[K]

  /** This is an implicit to allow overriding with [[implicits.optimizations]].*/
  implicit class MatrixBuilderSyntax(companion: Matrix.type) {
    def apply[M, N]: Matrix[Matrix.Expr]#Builder[M, N] =
      Matrix.unoptimized.builder[M, N]
  }

  implicit def unoptimized: Matrix[Expr] =
    new Matrix[Expr] {

      def map[
          K: Field,
          B: DenseBuffer.Aux[K, ?],
          M: ValueOf: ? <:< Int,
          N: ValueOf: ? <:< Int
      ](fk: Expr[K, M, N])(f: K => K): Expr[K, M, N] =
        (out, layout, stride) => {
          val m: Int = valueOf[M]
          val n: Int = valueOf[N]

          def go(m: Int, n: Int) = {
            val ld = stride.getOrElse(m)
            var y =
              if (out.isNull) DenseBuffer[K].allocateDirect(ld * n) else out
            y = unsafeToBuffer(fk, y.toNio, layout, stride)
            for (j <- 0 until n)
              for (i <- 0 until m)
                y.put(i + j * ld, f(y.get(i + j * ld)))
            y
          }

          if (layout == colMajor) go(m, n) else go(n, m)
        }

      /** @param origin The focal point index; defaults to `(0,0)`.
        * @param default The value to supply when the stencil falls outside `ff`.
        */
      protected[blas] def coflatMap[
          K: Field,
          B: DenseBuffer.Aux[K, ?],
          M: ValueOf: ? <:< Int,
          N: ValueOf: ? <:< Int,
          P: ValueOf: ? <:< Int,
          Q: ValueOf: ? <:< Int
      ](
          fk: Expr[K, M, N]
      )(ff: Expr[K, P, Q] => K, origin: (Int, Int)): Expr[K, M, N] =
        (out, layout, stride) => {
          def go(m: Int, n: Int, p: Int, q: Int, origin: (Int, Int)) = {
            val ldm = stride.getOrElse(m)
            var y =
              if (out.isNull) DenseBuffer[K].allocateDirect(ldm * n) else out
            y = unsafeToBuffer(fk, y.toNio, layout, stride)
            for (j <- 0 until n)
              for (i <- 0 until m)
                y.put(
                  i = j * ldm,
                  ff { (out, _, stride) =>
                    val ldp = stride getOrElse p
                    val x =
                      if (out.isNull)
                        DenseBuffer[K].allocateDirect(ldp * q)
                      else out
                    for (l <- 0 until q)
                      for (k <- 0 until p)
                        x.put(
                          k + l * ldp,
                          try {
                            y.get(
                              (i + k - origin._1) + (j * ldm + l - origin._2)
                            )
                          } catch {
                            case _: java.lang.IndexOutOfBoundsException =>
                              Field[K].zero
                          }
                        )
                    x
                  }
                )
            y
          }

          val m: Int = valueOf[M]
          val n: Int = valueOf[N]
          val p: Int = valueOf[P]
          val q: Int = valueOf[Q]

          if (layout == colMajor) go(m, n, p, q, origin)
          else go(n, m, q, p, origin.swap)
        }

      def unsafeToBuffer[
          K: Field,
          B: DenseBuffer.Aux[K, ?],
          M: ValueOf: ? <:< Int,
          N: ValueOf: ? <:< Int
      ](
          f: Expr[K, M, N],
          b: B,
          layout: Layout,
          stride: Option[Int]
      ): B =
        f(b, layout, stride).toNio

      def builder[M, N] = new Builder[M, N] {

        def fromBuffer[K, B](
            x: Buffer[K],
            layout: Layout,
            stride: Option[Int],
            uplo: Option[UpLo],
            diag: Option[Diag],
            packed: Boolean,
            banded: Boolean
        )(
            implicit
            K: Field[K],
            buff: DenseBuffer.Aux[K, B],
            cblas: CBlas.Aux[K, B],
            M: ValueOf[M],
            N: ValueOf[N],
            MToInt: M <:< Int,
            NToInt: N <:< Int
        ): Expr[K, M, N] = {
          import cblas._
          import spire.std.int._
          val m: Int = valueOf[M]
          val n: Int = valueOf[N]

          if (n.isOne) {
            require(
              (layout == rowMajor) || stride.forall(_ < m),
              s"""Invalid stride ${stride.get} is less than leading
                       |column dimension $m. Possible cause: if you want to
                       |create a strided vector, use layout = 
                       |rowMajor instead""".stripMargin
            )
            require(
              (layout == colMajor) || stride.forall(_ * m == x.size),
              s"Invalid stride ${stride.get} for buffer of size ${x.size}"
            )
            (output, layoutOut, strideOutOpt) => {
              val incIn = stride getOrElse 1
              val incOut = strideOutOpt getOrElse 1
              if (output == null && incIn == incOut) x
              else {
                val y =
                  if (output == null) buff allocateDirect (m * incOut)
                  else output
                copy(m, x.toNio, incIn, y.toNio, incOut)
                y
              }
            }
          } else if (uplo.isEmpty && diag.isEmpty && banded == false) {
            val strideIn: Int = stride getOrElse {
              x.size / (if (layout == colMajor) n else m)
            }
            require(
              ((layout == colMajor) && n * strideIn == x.size) || m * strideIn == x.size,
              s"Input size (${x.size}) is not $m×$n and/or not a multiple of" +
                "stride."
            )
            (output, layoutOut, strideOutOpt) => {
              val strideOut: Int = strideOutOpt getOrElse {
                if (layoutOut == colMajor) m else n
              }

              if ((layout == layoutOut) && (output == null) && (strideOut == strideIn))
                x
              else {
                val y =
                  if (output == null)
                    buff.allocateDirect(
                      strideOut * (if (layoutOut == colMajor) n else m)
                    )
                  else output
                if ((layout == layoutOut) && (strideIn.isOne && strideOut.isOne))
                  copy(m * n, x.toNio, strideIn, y.toNio, strideOut)
                else { // TODO: optimize
                  def mkIdx(l: Layout): (Int, Int, Int) => Int =
                    if (l == colMajor)(i, j, s) => i + j * s
                    else (i, j, s) => j + i * s
                  val iOut = mkIdx(layoutOut)
                  val iIn = mkIdx(layout)
                  for (j <- 0 until n)
                    for (i <- 0 until m)
                      y.put(iOut(i, j, strideOut), x.get(iIn(i, j, strideIn)))
                }
                y
              }
            }
          } else ??? //TODO handle triangular matrices.
        }
      }
    }

  def unapplySeq[K, B, M, N, F[_, _, _]](xs: F[K, M, N])(
      implicit
      F: Matrix[F],
      K: Field[K],
      buff: DenseBuffer.Aux[K, B],
      M: ValueOf[M],
      N: ValueOf[N],
      MisInt: M <:< Int,
      NisInt: N <:< Int
  ): Option[Seq[K]] =
    Some(F.toArray(xs).toSeq)

  final class Ops[F[_, _, _], K: Field, B: DenseBuffer.Aux[K, ?], M: ValueOf: ? <:< Int, N: ValueOf: ? <:< Int](
      lhs: F[K, M, N]
  )(implicit ev: Matrix[F]) {
    //import spire.marcos.Ops
    def map(rhs: K => K): F[K, M, N] = ev.map(lhs)(rhs)
    def coflatMap[P: ValueOf: ? <:< Int, Q: ValueOf: ? <:< Int](
        rhs: F[K, P, Q] => K,
        origin: (Int, Int) = (0, 0)
    ): F[K, M, N] =
      ev.coflatMap[K, B, M, N, P, Q](lhs)(rhs, origin)
    def toArray: Array[K] = ev.toArray(lhs)
    def unsafeToArray(
        a: Array[K],
        layout: Layout = rowMajor,
        stride: Option[Int] = None
    ) = ev.unsafeToArray(lhs, a, layout, stride)
    def toBuffer: B = ev.toBuffer(lhs)
    def unsafeToBuffer(
        b: B,
        layout: Layout = rowMajor,
        stride: Option[Int] = None
    ): B = ev.unsafeToBuffer(lhs, b, layout, stride)
  }

  trait Syntax {
    implicit def toMatrixAllOps[F[_, _, _]: Matrix, K: Field, B: DenseBuffer.Aux[
      K,
      ?
    ], M: ValueOf: ? <:< Int, N: ValueOf: ? <:< Int](
        self: F[K, M, N]
    ): Matrix.Ops[F, K, B, M, N] = new Matrix.Ops[F, K, B, M, N](self)
  }

  object ops extends Syntax
}
