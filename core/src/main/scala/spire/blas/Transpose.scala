package spire.blas

import cats.kernel.Monoid

import scala.annotation.tailrec

object Transpose extends Enumeration { self =>
  type Transpose = Value
  val noTrans = Value(CblasLibrary.CBLAS_TRANSPOSE.CblasNoTrans)
  val trans = Value(CblasLibrary.CBLAS_TRANSPOSE.CblasTrans)
  val conjTrans = Value(CblasLibrary.CBLAS_TRANSPOSE.CblasConjTrans)
  private[blas] val conj = Value(-conjTrans.id)

  implicit val monoid: Monoid[Transpose] = new Monoid[Transpose] {
    def empty = noTrans
    @tailrec def combine(x: Transpose, y: Transpose): Transpose =
      if (x == noTrans) y
      else if (x == trans) {
        if (x == trans) noTrans
        else if (y == conjTrans) conj
        else if (y == conj) conjTrans
        else combine(y, x)
      } else if (x == conjTrans) {
        if (y == conjTrans) trans
        else if (x == conj) noTrans
        else combine(y, x)
      } else noTrans // x == y == conj
  }
}
