package spire.blas

object Diag extends Enumeration { self =>
  type Diag = Value
  val nonUnit = Value(CblasLibrary.CBLAS_DIAG.CblasNonUnit)
  val unit = Value(CblasLibrary.CBLAS_DIAG.CblasUnit)
}
