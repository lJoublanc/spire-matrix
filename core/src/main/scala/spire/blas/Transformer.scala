package spire.blas

/** This is trait is used for stacking optimizations.  refer to
  * [[http://okmij.org/ftp/tagless-final/course/optimizations.html]] for an
  * explanation of how these work.  */
protected trait Transformer {

  /** The tagless final encoding + context infromation.  */
  type Term

  /** The original encoding i.e. that we are optimizing. */
  type From

  /** Lift our original encoding into the optimization type. */
  def pure(x: From): Term

  /** Evaluate the optimization, returning a value of our original encoding. */
  def eval(t: Term): From
}
