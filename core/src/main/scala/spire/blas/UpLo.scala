package spire.blas

object UpLo extends Enumeration { self =>
  type UpLo = Value
  val upper = Value(CblasLibrary.CBLAS_UPLO.CblasUpper)
  val lower = Value(CblasLibrary.CBLAS_UPLO.CblasLower)
}
