package spire.blas

object Layout extends Enumeration { self =>
  type Layout = Value
  val rowMajor = Value(CblasLibrary.CBLAS_ORDER.CblasRowMajor)
  val colMajor = Value(CblasLibrary.CBLAS_ORDER.CblasColMajor)
}
