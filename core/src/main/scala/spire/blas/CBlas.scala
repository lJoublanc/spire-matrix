package spire.blas

import CblasLibrary._
import CBlas.DenseTypes

import java.nio
import scala.{specialized => sp}

/** A Generic interface to [[CblasLibrary]]. */
trait CBlas[@sp(DenseTypes) T] {
  import CBlas.Inc

  type Buffer

  /** Unlike scala's `arraycopy`, this accounts for strides. */
  def copy: (Int, Buffer, Inc, Buffer, Inc) => Unit

  def scal: (Int, T, Buffer, Inc) => Unit

  def axpy: (Int, T, Buffer, Inc, Buffer, Inc) => Unit

  // format: off
  def gemm: (Int, Int, Int, Int, Int, Int, T, Buffer, Inc, Buffer, Inc, T, Buffer, Inc) => Unit

  def symm: (Int, Int, Int, Int, Int, T, Buffer, Inc, Buffer, Inc, T, Buffer, Inc) => Unit

  def trmm: (Int, Int, Int, Int, Int, Int, Int, T, Buffer, Inc, Buffer, Inc) => Unit

  def trsm: (Int, Int, Int, Int, Int, Int, Int, T, Buffer, Inc, Buffer, Inc) => Unit
  // format: on
}

object CBlas {

  protected type Inc = Int

  type Aux[T, B] = CBlas[T] { type Buffer = B }

  type Xau[B, T] = Aux[T, B]

  implicit lazy val denseDouble: CBlas.Aux[Double, nio.DoubleBuffer] =
    new CBlas[Double] {

      type Buffer = nio.DoubleBuffer

      val copy = cblas_dcopy
      val scal = cblas_dscal
      val axpy = cblas_daxpy
      val gemm = cblas_dgemm
      val symm = cblas_dsymm
      val trmm = cblas_dtrmm
      val trsm = cblas_dtrsm
    }

  /** Used for specialization of interfaces */
  final val DenseTypes = new scala.Specializable.Group((Float, Double))
}
