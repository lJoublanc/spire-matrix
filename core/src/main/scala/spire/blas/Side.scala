package spire.blas

object Side extends Enumeration { self =>
  protected case class Val(id: Int) extends Value {
    def flip: Val = if (id == left.id) right else left
  }
  type Side = Val
  val left = Val(CblasLibrary.CBLAS_SIDE.CblasLeft)
  val right = Val(CblasLibrary.CBLAS_SIDE.CblasRight)
}
