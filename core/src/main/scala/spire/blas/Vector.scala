package spire.blas

import spire.algebra.Field
import spire.blas.CBlas.DenseTypes

import scala.{specialized => sp}

/** Alias for one-dimensional column vector. */
object Vector {

  /** This is an extension method so it can be overridden by importing
    * [[implicits.optimizations]]. */
  implicit class VectorBuilderSyntax(companion: Vector.type) {
    def apply[M]: Matrix[Matrix.Expr]#Builder[M, 1] =
      Matrix.unoptimized.builder[M, 1]
  }

  def unapplySeq[
      F[_, _, _]: Matrix,
      K: Field,
      B: DenseBuffer.Aux[K, ?],
      M: ValueOf: ? <:< Int
  ](
      xs: F[K, M, 1]
  ): Option[Seq[K]] =
    Some(implicitly[Matrix[F]].toArray(xs).toSeq)
}
