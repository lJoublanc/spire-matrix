package spire.blas.implicits

import spire.algebra.{VectorSpace, Field, LinearMap, LinearGroup, Eq}
import spire.blas._
import spire.blas.DenseBuffer.Buffer
import spire.blas.DenseBuffer.ops._
import spire.blas.Layout._
import spire.blas.Transpose._
import spire.blas.UpLo._
import spire.blas.Diag._
import spire.blas.Side._
import spire.syntax.field._

package object optimizations extends Optimizations

protected trait Optimizations extends HighPrioTransforms {

  implicit class VectorBuilderSyntax(companion: Vector.type) {
    def apply[M]: Matrix[BlasCtx.Expr]#Builder[M, 1] =
      optimizedMatrixInstance.builder[M, 1]
  }

  implicit class MatrixBuilderSyntax(companion: Matrix.type) {
    def apply[M, N]: Matrix[BlasCtx.Expr]#Builder[M, N] =
      optimizedMatrixInstance.builder[M, N]
  }

  implicit def optimizedMatrixInstance(
      implicit unopt: Matrix[Matrix.Expr]
  ): Matrix[BlasCtx.Expr] =
    new Matrix[BlasCtx.Expr] {
      import BlasCtx._

      def map[
          K: Field,
          B: DenseBuffer.Aux[K, ?],
          M: ValueOf: ? <:< Int,
          N: ValueOf: ? <:< Int
      ](fk: Expr[K, M, N])(ff: K => K): Expr[K, M, N] =
        //TODO: consider deferring if it's a transpose?
        ctx => f => unopt.map(fk(ctx)(f))(ff)

      protected[blas] def coflatMap[
          K: Field,
          B: DenseBuffer.Aux[K, ?],
          M: ValueOf: ? <:< Int,
          N: ValueOf: ? <:< Int,
          P: ValueOf: ? <:< Int,
          Q: ValueOf: ? <:< Int
      ](
          fk: Expr[K, M, N]
      )(ff: Expr[K, P, Q] => K, origin: (Int, Int)): Expr[K, M, N] = {
        import cats.Contravariant
        import cats.instances.function._
        val gg: Matrix.Expr[K, P, Q] => K =
          Contravariant[? => K]
            .contramap(ff)(matExp => Unknown => ??? => matExp)
        ctx => f => unopt.coflatMap[K, B, M, N, P, Q](fk(ctx)(f))(gg, origin)
      }

      def unsafeToBuffer[
          K: Field,
          B: DenseBuffer.Aux[K, ?],
          M: ValueOf: ? <:< Int,
          N: ValueOf: ? <:< Int
      ](
          f: BlasCtx.Expr[K, M, N],
          b: B,
          layout: Layout,
          stride: Option[Int]
      ): B =
        unopt.unsafeToBuffer(
          f(Unknown)(
            (_, _, _, _, _, _, _, _, _) => throw new Error("Dead code")
          ),
          b,
          layout,
          stride
        )

      def builder[M, N]: Builder[M, N] = new Builder[M, N] {
        def fromBuffer[K, B](
            x: Buffer[K],
            layout: Layout,
            stride: Option[Int],
            uplo: Option[UpLo],
            diag: Option[Diag],
            packed: Boolean,
            banded: Boolean
        )(
            implicit
            K: Field[K],
            buff: DenseBuffer.Aux[K, B],
            cblas: CBlas.Aux[K, B],
            M: ValueOf[M],
            N: ValueOf[N],
            MToInt: M <:< Int,
            NToInt: N <:< Int
        ): BlasCtx.Expr[K, M, N] = {
          val A =
            unopt
              .builder[M, N]
              .fromBuffer(x, layout, stride, uplo, diag, packed, banded)

          lazy val z: Matrix.Expr[K, M, N] = (_, _, _) =>
            throw new Error("Identity")

          {
            case Unknown => _ => A
            case Summand | Scale | Product | Transpose | Invert =>
              f =>
                f(
                  left,
                  valueOf[N],
                  Field[K].one,
                  A,
                  noTrans,
                  false,
                  z,
                  noTrans,
                  true
                )
          }
        }
      }
    }
}

protected trait HighPrioTransforms extends LowPrioTransforms {

  implicit def level1Transform[
      K: Eq,
      B,
      M: ? <:< Int: ValueOf,
      N: ? =:= 1
  ](
      implicit
      B: DenseBuffer.Aux[K, B],
      cblas: CBlas.Aux[K, B],
      K: Field[K],
      V: VectorSpace[Matrix.Expr[K, M, 1], K]
  ): VectorSpace[BlasCtx.Expr[K, M, N], K] =
    new VectorSpace[BlasCtx.Expr[K, M, N], K] with BlasCtxTransformer[K, M, 1] {
      import cblas._
      import BlasCtx._

      lazy val m: Int = valueOf[M]

      def negate(x: Term): Term =
        timesl(K.negate(K.one), x)(_)

      def zero: Term =
        pure(V.zero)

      def plus(x: Term, y: Term): Term =
        pure {
          x(Summand) { (_, _, α, x, _, _, _, _, yIsNull) =>
            y(Summand) { (_, _, β, y, _, _, _, _, vIsNull) =>
              assert(yIsNull && vIsNull)
              def canonicalOp(α: K, x: From, y: From): From =
                (out, layout, inc) => {
                  var Y =
                    if (out.isNull) alloc(layout, inc, m, 1)
                    else out
                  Y = y.unsafeToBuffer(Y.toNio, layout, inc)
                  val X = x.toBuffer
                  axpy(m, α, X.toNio, X.size / m, Y.toNio, Y.size / m)
                  Y
                }
              if (α.isOne) canonicalOp(β, y, x)
              else if (β.isOne) canonicalOp(α, x, y)
              else canonicalOp(α, x, V.timesl(β, y))
            }
          }
        }

      def timesl(k: K, v: Term): Term = {
        case Summand =>
          f => f(left, -1, k, eval(v), noTrans, false, V.zero, noTrans, true)
        case Unknown => _ => V.timesl(k, eval(v))
      }

      def scalar: Field[K] = K
    }

  implicit def blasLevel3LinearGroupTransform[K: Eq, B](
      implicit K: Field[K],
      G: LinearGroup[λ[m => Matrix.Expr[K, m, m]]]
  ): LinearGroup[λ[m => BlasCtx.Expr[K, m, m]]] =
    new Level3LinearMapTransform[K, B]
    with LinearGroup[λ[m => BlasCtx.Expr[K, m, m]]] {
      import BlasCtx._
      def inverse[M: ValueOf: ? <:< Int](x: Term[M, M]): Term[M, M] = {
        case Invert  => _ => eval(x)
        case Unknown => _ => G.inverse(eval(x))
        case Summand | Scale | Product | Transpose | Hermitian =>
          f =>
            x(Invert) { (side, nA, α, A, transa, inva, B, transb, bIsOne) =>
              val m = valueOf[M]
              if (bIsOne)
                f(side, m, K.reciprocal(α), A, transa, !inva, B, transb, true)
              else // (AB)⁻¹ == B⁻¹A⁻¹
                f(
                  side.flip,
                  nA,
                  K.reciprocal(α),
                  A,
                  transa,
                  !inva,
                  G.inverse(B),
                  transb,
                  false
                )
            }
      }
    }
}

protected trait LowPrioTransforms {

  implicit def level3VectorSpaceTransform[
      K: Eq,
      B,
      M: ? <:< Int: ValueOf,
      N: ? <:< Int: ValueOf
  ](
      implicit
      buff: DenseBuffer.Aux[K, B],
      cblas: CBlas.Aux[K, B],
      K: Field[K],
      V: VectorSpace[Matrix.Expr[K, M, N], K]
  ): VectorSpace[BlasCtx.Expr[K, M, N], K] =
    new VectorSpace[BlasCtx.Expr[K, M, N], K] with BlasCtxTransformer[K, M, N] {
      import cblas.{Buffer => _, _}
      import BlasCtx._

      lazy val m: Int = valueOf[M]

      lazy val n: Int = valueOf[N]

      private def getStride(layout: Layout, b: Buffer[K]) =
        b.size / (if (layout == colMajor) n else m)

      def negate(x: Term): Term =
        timesl(K.negate(K.one), x)(_)

      def zero: Term = pure(V.zero)

      def plus(x: Term, y: Term): Term = {
        def canonicalOp(
            α: K,
            a: From,
            nA: Int,
            transa: Transpose,
            inva: Boolean,
            b: From,
            transb: Transpose,
            β: K,
            c: From
        ): From = (outC, layout, strideC) => {
          val A = a.toBuffer
          val B = b.toBuffer
          var C =
            if (outC.isNull) alloc(layout, strideC, m, n)
            else outC
          C = c.unsafeToBuffer(C.toNio, layout, strideC)
          val lda = getStride(layout, A)
          val ldb = getStride(layout, B)
          val ldc = getStride(layout, C)
          gemm(
            layout.id,
            transa.id,
            transb.id,
            m,
            n,
            nA,
            α,
            A.toNio,
            lda,
            B.toNio,
            ldb,
            β,
            C.toNio,
            ldc
          )
          C
        }

        pure {
          x(Summand) { (_, nA, α, a, transa, inva, b, transb, bIsOne) =>
            y(Summand) { (_, nC, β, c, transc, invc, d, transd, dIsOne) =>
              if (dIsOne && !bIsOne && (transc == noTrans) && !invc)
                canonicalOp(α, a, nA, transa, inva, b, transb, β, c)
              else if (bIsOne && !dIsOne && (transa == noTrans) && !inva)
                canonicalOp(β, c, nC, transc, invc, d, transd, α, a)
              else
                V.plus(eval(x), eval(y)) //TODO: further optimization - multiply out AB or CD
            }
          }
        }
      }

      def timesl(a: K, x: Term): Term = {
        case Summand =>
          f =>
            x(Scale) { (side, k, α, A, transa, inva, B, transb, bIsI) =>
              f(side, k, α * a, A, transa, inva, B, transb, bIsI)
            }
        case _ => _ => V.timesl(a, eval(x))
      }

      def scalar: Field[K] = K
    }

  implicit def level3LinearMapTransform[K: Eq, B](
      implicit
      K: Field[K],
      F: LinearMap[Matrix.Expr[K, ?, ?]],
      G: LinearGroup[λ[m => Matrix.Expr[K, m, m]]]
  ): LinearMap[BlasCtx.Expr[K, ?, ?]] =
    new Level3LinearMapTransform()(Eq[K], K, F, G)

  protected class Level3LinearMapTransform[K: Eq, B](
      implicit
      K: Field[K],
      F: LinearMap[Matrix.Expr[K, ?, ?]],
      G: LinearGroup[λ[m => Matrix.Expr[K, m, m]]]
  ) extends LinearMap[BlasCtx.Expr[K, ?, ?]] {
    import BlasCtx._

    type Term[m, n] = BlasCtx.Expr[K, m, n]

    type From[m, n] = Matrix.Expr[K, m, n]

    def eval[M, N](t: Term[M, N]): From[M, N] = {
      t(Unknown)(
        (_, _, _, _, _, _, _, _, _) => throw new Exception("Dead Code")
      )
    }

    def mmult[
        M: ValueOf: ? <:< Int,
        T: ValueOf: ? <:< Int,
        N: ValueOf: ? <:< Int
    ](x: Term[M, T], y: Term[T, N]): Term[M, N] = {
      case Scale | Summand =>
        f =>
          x(Product) { (_, nA, α, A, transa, inva, B, transb, bIsOne) =>
            y(Product) { (_, nB, β, C, transc, invc, D, transd, dIsOne) =>
              if (bIsOne && dIsOne && !invc)
                f(left, nA, α * β, A, transa, inva, C, transc, false)
              else
                F.mmult[M, T, N](eval(x), eval(y)) //TODO: rearrange so matrix sizes are the correct.
            }
          }
      case _ => _ => F.mmult[M, T, N](eval(x), eval(y))
    }

    private def xpose[M: ValueOf: ? <:< Int, N](
        t: Transpose
    )(x: Term[M, N]): Term[Int, Int] = {
      import cats.syntax.monoid._
      assert(t == trans || t == conjTrans || t == conj)
      lazy val m: Int = valueOf[M]
      ctx =>
        f =>
          x(ctx) { (side, nA, α, A, transa, inva, B, transb, bIsOne) =>
            val sa = t |+| transa
            lazy val sb = t |+| transb
            val transa2 = if (sa == conj) noTrans else sa
            lazy val transb2 = if (sb == conj) noTrans else sb
            val a = if ((sa == conj) != (sb == conj)) -α else α

            if (bIsOne) f(side, m, a, A, transa2, inva, B, noTrans, true)
            else
              f(
                side.flip,
                nA,
                a,
                if (inva) G.inverse(A) else A,
                transa2,
                false,
                B,
                transb2,
                false
              )
          }
    }

    def transpose[M: ValueOf: ? <:< Int, N: ValueOf: ? <:< Int](
        x: Term[M, N]
    ): Term[N, M] = xpose(trans)(x)

    override def adjoint[M: ValueOf: ? <:< Int, N: ValueOf: ? <:< Int](
        x: Term[M, N]
    ): Term[M, N] = xpose(conj)(x)

    override def hermitian[M: ValueOf: ? <:< Int, N: ValueOf: ? <:< Int](
        x: Term[M, N]
    ): Term[N, M] = xpose(conjTrans)(x)

    /** The identity operator. */
    def id[N: ValueOf: ? <:< Int]: Term[N, N] = ???
  }
}
