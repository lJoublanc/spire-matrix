package spire.blas

import spire.algebra.{VectorSpace, Field, LinearMap, LinearGroup, Eq}
import spire.blas.Transpose._
import spire.blas.Layout._
import spire.blas.DenseBuffer.ops._
import spire.blas.DenseBuffer.Buffer
import spire.blas.Matrix.ops._
import spire.syntax.field._

import java.nio

package object implicits extends Implicits with Matrix.Syntax with ShowMatrix

protected trait Implicits extends LowPrioImplicits {

  //implicit def blasSquareLevel3VectorSpace //specialized for UPLO, diag etc.
  /** A vector space specialized for BLAS Level 1 (i.e. vector-only) operations. */
  implicit def level1VectorSpace[
      K: Eq,
      B,
      M: ? <:< Int: ValueOf,
      N: ? =:= 1
  ](
      implicit
      buff: DenseBuffer.Aux[K, B],
      cblas: CBlas.Aux[K, B],
      K: Field[K]
  ): VectorSpace[Matrix.Expr[K, M, N], K] =
    new VectorSpace[Matrix.Expr[K, M, N], K] {
      import Matrix.Expr
      import Matrix.ops._
      import cblas.{Buffer => _, _}

      protected lazy val m: Int = valueOf[M]

      def scalar = K

      def negate(f: Expr[K, M, 1]): Expr[K, M, 1] =
        timesl(K.negate(K.one), f)

      def zero: Expr[K, M, 1] =
        (out, layout, stride) => buff.allocateDirect(m * (stride getOrElse 1))

      def timesl(a: K, f: Expr[K, M, 1]): Expr[K, M, 1] =
        if (a.isOne) f
        else
          (out, layout, stride) => {
            val incx = stride getOrElse 1
            var x: Buffer[K] =
              if (out.isNull) DenseBuffer[K].allocateDirect(incx * m)
              else out
            x = f.unsafeToBuffer(x.toNio, layout, stride)
            scal(m, a, x.toNio, x.size / m)
            x
          }

      def plus(f: Expr[K, M, 1], g: Expr[K, M, 1]): Expr[K, M, 1] =
        (out, layout, stride) => {
          val incy = stride getOrElse 1
          val x = f.toBuffer
          var y =
            if (out.isNull) buff.allocateDirect(incy * m)
            else out
          y = g.unsafeToBuffer(y.toNio, layout, stride)
          axpy(m, K.one, x.toNio, x.size / m, y.toNio, incy)
          y
        }
    }
}

protected trait LowPrioImplicits {

  implicit val defaultEndianess: nio.ByteOrder = nio.ByteOrder.LITTLE_ENDIAN

  /** Allocates a buffer taking into acount stride (leading dimension) */
  private[blas] def alloc[K, B: DenseBuffer.Aux[K, ?]](
      l: Layout,
      s: Option[Int],
      m: => Int,
      n: => Int
  ): Buffer[K] =
    DenseBuffer[K].allocateDirect {
      if (l == colMajor) s.getOrElse(m) * n
      else s.getOrElse(n) * m
    }

  /** Matrix multiplication can be interpreted as linear composition. */
  implicit def linearMap[K, B](
      implicit cblas: CBlas.Aux[K, B],
      buff: DenseBuffer.Aux[K, B],
      K: Field[K]
  ): LinearMap[Matrix.Expr[K, ?, ?]] = new BlasLinearMap[K, B]

  protected class BlasLinearMap[K, B](
      implicit cblas: CBlas.Aux[K, B],
      buff: DenseBuffer.Aux[K, B],
      K: Field[K]
  ) extends LinearMap[Matrix.Expr[K, ?, ?]] {
    import Matrix.Expr
    import cblas.{Buffer => _, _}

    private def getStride(l: Layout, b: Buffer[K], m: => Int, n: => Int): Int =
      if (l == colMajor) b.size / n
      else b.size / m

    def mmult[
        M: ValueOf: ? <:< Int,
        T: ValueOf: ? <:< Int,
        N: ValueOf: ? <:< Int
    ](
        f: Expr[K, M, T],
        g: Expr[K, T, N]
    ): Expr[K, M, N] =
      (out, layout, ldc) => {
        val m: Int = valueOf[M]
        val n: Int = valueOf[N]
        val t: Int = valueOf[T]
        val A = f.toBuffer
        val B = g.toBuffer
        val C = if (out.isNull) alloc(layout, ldc, m, n) else out
        val lda: Int = getStride(layout, A, m, t)
        val ldb: Int = getStride(layout, B, t, n)

        gemm(
          layout.id,
          noTrans.id,
          noTrans.id,
          m,
          n,
          t,
          K.one,
          A.toNio,
          lda,
          B.toNio,
          ldb,
          K.zero,
          C.toNio,
          ldc getOrElse (if (layout == colMajor) m else n)
        )
        C
      }

    //TODO: should this be represented as a triangular/banded matrix?
    def id[N: ValueOf: ? <:< Int]: Expr[K, N, N] =
      (out, layout, stride) => {
        val n: Int = valueOf[N]
        val b = buff.allocateDirect((stride getOrElse n) * n)
        for (i <- 0 until n) b.put(i + n * i, K.one)
        b
      }

    def adjoint[M: ValueOf: ? <:< Int, N: ValueOf: ? <:< Int](
        f: Expr[K, M, N]
    ): Expr[K, M, N] = transpose(hermitian(f))

    def transpose[M: ValueOf: ? <:< Int, N: ValueOf: ? <:< Int](
        f: Expr[K, M, N]
    ): Expr[K, N, M] = blasTranspose(f, trans)

    override def hermitian[M: ValueOf: ? <:< Int, N: ValueOf: ? <:< Int](
        f: Expr[K, M, N]
    ): Expr[K, N, M] = blasTranspose(f, conjTrans)

    protected def blasTranspose[M: ValueOf: ? <:< Int, N: ValueOf: ? <:< Int](
        f: Expr[K, M, N],
        transA: Transpose
    ): Expr[K, N, M] = (out, layout, ldc) => {
      val m: Int = valueOf[M]
      val n: Int = valueOf[N]
      val A = f.toBuffer
      val lda = getStride(layout, A, m, n)
      val C =
        if (out.isNull) alloc(layout, ldc orElse Some(lda), m, n)
        else out
      gemm(
        layout.id,
        transA.id,
        noTrans.id,
        m,
        n,
        n,
        K.one,
        A.toNio,
        lda,
        id[N].toBuffer.toNio,
        n,
        K.zero,
        C.toNio,
        ldc getOrElse lda
      )
      C
    }

  }

  implicit def linearGroup[K, B](
      implicit cblas: CBlas.Aux[K, B],
      buff: DenseBuffer.Aux[K, B],
      K: Field[K]
  ): LinearGroup[λ[m => Matrix.Expr[K, m, m]]] =
    new BlasLinearMap[K, B] with LinearGroup[λ[m => Matrix.Expr[K, m, m]]] {
      import Matrix.Expr

      def inverse[M: ValueOf: ? <:< Int](
          f: Expr[K, M, M]
      ): Expr[K, M, M] =
        (outB, layout, ldb) => {
          import Side._
          import Diag._
          import UpLo._
          val m: Int = valueOf[M]
          val A = f.toBuffer
          val B = id[M].unsafeToBuffer(outB.toNio, layout, ldb)
          val uploSize = m * (m + 1) / 2
          val isDiag = if (uploSize + m == A.size) unit else nonUnit
          val isUplo = uploSize == A.size || isDiag == unit
          assert(
            isUplo,
            "Inversion only supported for non-strided, triangular matrices currently."
          )
          cblas.trsm(
            layout.id,
            upper.id, // FIXME: How do we know if it's upper or lower triangular?
            left.id,
            noTrans.id,
            isDiag.id,
            m,
            m,
            K.one,
            A.toNio,
            A.size / (m + 1) * 2,
            B.toNio,
            ldb getOrElse m
          )
          B
        }
    }

  /** A vector space for operations on general matrices.
    * Unlike [[level1VectorSpace]] this handles strided matrices.
    */
  implicit def blasGeneralLevel3VectorSpace[
      K: Eq,
      B,
      M: ? <:< Int: ValueOf,
      N: ? <:< Int: ValueOf
  ](
      implicit
      buff: DenseBuffer.Aux[K, B],
      cblas: CBlas.Aux[K, B],
      K: Field[K]
  ): VectorSpace[Matrix.Expr[K, M, N], K] =
    new VectorSpace[Matrix.Expr[K, M, N], K] {
      import Matrix.Expr
      import cblas.{Buffer => _, _}

      protected lazy val m: Int = valueOf[M]

      protected lazy val n: Int = valueOf[N]

      @inline private def z: B = null.asInstanceOf[B]

      /** Infer stride from `B`. Note that `B` is a general dense (i.e. not
        * triangular or banded) matrix, hence we can do this. */
      private def getStride(layout: Layout, b: Buffer[K]) =
        b.size / (if (layout == colMajor) n else m)

      def scalar = K

      def negate(f: Expr[K, M, N]): Expr[K, M, N] =
        timesl(K.negate(K.one), f)

      def zero: Expr[K, M, N] =
        (out, layout, stride) =>
          if (out.isNull) alloc(layout, stride, m, n)
          else {
            if (layout == colMajor)
              for (j <- 0 until n)
                for (i <- 0 until m)
                  out.put(i * j * stride.getOrElse(m), K.zero)
            else
              for (i <- 0 until m)
                for (j <- 0 until n)
                  out.put(j * i * stride.getOrElse(n), K.zero)
            out
          }

      def timesl(α: K, f: Expr[K, M, N]): Expr[K, M, N] =
        if (α.isOne) f
        else
          (out, layout, stride) => {
            var B =
              if (out.isNull) alloc(layout, stride, m, n)
              else out
            B = f.unsafeToBuffer(B.toNio, layout, stride)
            val ldb = getStride(layout, B)
            if ((layout == colMajor && ldb == m) || ldb == n)
              scal(m * n, α, B.toNio, 1)
            else
              gemm(
                layout.id,
                noTrans.id,
                noTrans.id,
                m,
                n,
                n,
                K.zero,
                z,
                m,
                z,
                m,
                α,
                B.toNio,
                stride getOrElse getStride(layout, B)
              )
            B
          }

      def plus(f: Expr[K, M, N], g: Expr[K, M, N]): Expr[K, M, N] =
        (outC, layout, ldc) => {
          import Side._
          import UpLo._
          lazy val I: Buffer[K] = {
            val x = buff.allocateDirect(n * n)
            for (i <- 0 until n) x.put(n * i + i, K.one)
            x
          }
          val B = f.toBuffer
          lazy val ldb = getStride(layout, B)
          var C =
            if (outC.isNull) alloc(layout, ldc, m, n)
            else outC
          C = g.unsafeToBuffer(C.toNio, layout, ldc)
          if ((layout == colMajor && ldb == m && ldc.forall(_ == n)) ||
              (ldb == n && ldc.forall(_ == m)))
            axpy(m * n, K.one, B.toNio, 1, C.toNio, 1)
          else // TODO Find a way to avoid allocating I
            symm(
              layout.id,
              right.id, //I is uplo
              upper.id,
              m,
              n,
              K.one,
              I.toNio,
              n,
              B.toNio,
              ldb,
              K.one,
              C.toNio,
              ldc getOrElse getStride(layout, C)
            )
          C
        }
    }
}
