package spire.blas

import Side._
import Transpose._

sealed trait BlasCtx

object BlasCtx {
  case object Summand extends BlasCtx
  case object Scale extends BlasCtx
  case object Product extends BlasCtx
  case object Transpose extends BlasCtx
  case object Hermitian extends BlasCtx
  case object Invert extends BlasCtx
  case object Unknown extends BlasCtx
  type Expr[K, M, N] =
    BlasCtx => (
        (
            Side, // if right then premultiply instead of postmult
            Int, // k i.e. leading dim of B
            K, // α
            Matrix.Expr[K, M, Int], // A
            Transpose,
            Boolean, // if A is inversed
            Matrix.Expr[K, Int, N], // B
            Transpose,
            Boolean // if B is I
        ) => Matrix.Expr[K, M, N]
    ) => Matrix.Expr[K, M, N]
}
