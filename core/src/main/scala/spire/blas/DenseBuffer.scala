package spire.blas

import java.nio
import scala.reflect.ClassTag

/** Typeclass to make `java.nio.Buffer generic. */
trait DenseBuffer[T] {
  import DenseBuffer.Buffer

  type Nio

  @inline def toNio(b: Buffer[T]): Nio = b.asInstanceOf[Nio]

  @inline def fromNio[B](b: B): Buffer[T] = b.asInstanceOf[Buffer[T]]

  def endianess: nio.ByteOrder

  def allocateDirect(i: Int): Buffer[T]

  def wrap(array: Array[T]): Buffer[T]

  def put(b: Buffer[T])(i: Int, t: T): Buffer[T]

  def get(b: Buffer[T])(i: Int): T

  def slice(b: Buffer[T])(start: Int, count: Int): Buffer[T]

  def toArray(b: Buffer[T]): Array[T]

  def isNull(b: Buffer[T]): Boolean = b == null

  def size(b: Buffer[T]): Int
}

object DenseBuffer {

  /* This monstrosity is solely used to treat nio.xxxBuffer as an F[_], so we
   * don't need an additional `B` type parameter for `Matrix.Expr`. */
  protected[blas] type Buffer[_] = Any

  type Aux[T, B] = DenseBuffer[T] { type Nio = B }

  type Xau[B, T] = Aux[T, B]

  def apply[T](implicit B: DenseBuffer[T]): DenseBuffer.Aux[T, B.Nio] = B

  trait Ops[T, B] {

    protected def tc: DenseBuffer.Aux[T, B]

    protected def self: Buffer[T]

    def toNio: B = tc.toNio(self)

    def put(i: Int, t: T): Buffer[T] = tc.put(self)(i, t)

    def get(i: Int): T = tc.get(self)(i)

    def slice(start: Int, count: Int): Buffer[T] = tc.slice(self)(start, count)

    def toArray: Array[T] = tc.toArray(self)

    def isNull: Boolean = tc.isNull(self)

    def size: Int = tc.size(self)
  }

  object ops {
    implicit def bufferSyntax[T, B](
        target: Buffer[T]
    )(implicit typeclass: DenseBuffer.Aux[T, B]): Ops[T, B] = new Ops[T, B] {
      val tc = typeclass
      val self = target
    }
  }

  implicit def denseSingle(
      implicit bo: nio.ByteOrder
  ): DenseBuffer.Aux[Float, nio.FloatBuffer] =
    nioBufferInstance(
      java.lang.Float.BYTES,
      _.asFloatBuffer,
      _.get,
      _.get,
      _.put,
      _.slice,
      nio.FloatBuffer.wrap
    )

  implicit def denseDouble(
      implicit bo: nio.ByteOrder
  ): DenseBuffer.Aux[Double, nio.DoubleBuffer] =
    nioBufferInstance(
      java.lang.Double.BYTES,
      _.asDoubleBuffer,
      _.get,
      _.get,
      _.put,
      _.slice,
      nio.DoubleBuffer.wrap
    )

  def nioBufferInstance[T: ClassTag, B <: nio.Buffer](
      _sizeBytes: Int,
      _fromBytes: nio.ByteBuffer => B,
      _get: B => Int => T,
      _getArr: B => Array[T] => B,
      _put: B => (Int, T) => B,
      _slice: B => B,
      _wrap: Array[T] => B
  )(
      implicit bo: nio.ByteOrder
  ): DenseBuffer.Aux[T, B] =
    new DenseBuffer[T] {

      type Nio = B

      def endianess: nio.ByteOrder = bo

      def allocateDirect(n: Int): Buffer[T] =
        _fromBytes(
          nio.ByteBuffer.allocateDirect(_sizeBytes * n).order(endianess)
        )

      def wrap(array: Array[T]): Buffer[T] = _wrap(array)

      def slice(b: Buffer[T])(start: Int, count: Int): Buffer[T] =
        _slice(toNio(toNio(b).position(start).limit(start + count)))

      def put(b: Buffer[T])(i: Int, t: T): Buffer[T] = _put(toNio(b))(i, t)

      def get(b: Buffer[T])(i: Int): T = _get(toNio(b))(i)

      def toArray(b: Buffer[T]): Array[T] =
        if (toNio(b).hasArray()) toNio(b).array().asInstanceOf[Array[T]]
        else {
          val a = Array.ofDim[T](size(b))
          _getArr(toNio(b))(a)
          a
        }
      def size(b: Buffer[T]): Int = toNio(b).capacity
    }

}
