package spire.blas

import spire.blas.DenseBuffer
import spire.algebra.Field
import cats.Show

import java.nio.charset.Charset
import java.nio.charset.StandardCharsets.{UTF_16, UTF_8}
import scala.collection.immutable.ArraySeq

object ShowMatrix {

  /** Use to override the default element separator when displaying matrices. */
  case class Separator(value: String) extends AnyVal

  /** Use to override the maximum width (# of cols) when displaying matrices. */
  case class MaxWidth(value: Int) extends AnyVal

  /** Use to override the maximum height (# of rows) when displaying matrices. */
  case class MaxHeight(value: Int) extends AnyVal
}

trait ShowMatrix {
  import ShowMatrix._

  /** Show instance optimized for console.
    * Supports unicode characters where available.
    * @param charset Used to determine whether to use special characters to
    *        print brackets.
    * @param separator This string will be placed between elements.
    * @param maxWidth Matrices with more than this number of columns will not be
    *                 displayed/evaluated.
    * @param maxHeight Matrices with more than this number of rows will not be
    *                  displayed/evaluated.
    * @note This is an effectful function; it will evalute the linear
    *       expression in order to display it.
    * @example {{{
    * import spire.blas.ShowMatrix._
    * implicit val comma = Separator(", ")
    * // comma: spire.blas.ShowMatrix.Separator = Separator(, )
    *
    * val x = Matrix[2,2](Seq(-1.0, 3.0/2.0, 1.0, -1.0): _*)
    * // x: spire.blas.BlasCtx.Expr[Double,java.nio.DoubleBuffer,2,2] = spire.blas.implicits.Optimizations$$anon$1$$anon$2$$Lambda$10975/239778571@7f2735
    *
    * scala> x.show
    * // res0: String =
    * // ┌          ┐
    * // │-1.0,  1.5│
    * // │ 1.0, -1.0│
    * // └          ┘
    * }}}
    */
  implicit def consoleShow[
      K: Show: Field,
      B,
      M: ValueOf: ? <:< Int,
      N: ValueOf: ? <:< Int,
      F[_, _, _]: Matrix
  ](
      implicit
      charset: Charset = Charset.defaultCharset(),
      separator: Separator = Separator(" "),
      maxWidth: MaxWidth = MaxWidth(80 / 6),
      maxHeight: MaxHeight = MaxHeight(80),
      buff: DenseBuffer.Aux[K, B]
  ): Show[F[K, M, N]] =
    (x: F[K, M, N]) => {
      import cats.syntax.show._
      import Matrix.ops._
      val (m, n) = (valueOf[M], valueOf[N])

      def row(s: Seq[String]): String = s.mkString(separator.value)

      if (n * m == 0) "[ Null Matrix (m x n = 0) ]"
      else if (n > maxWidth.value || m > maxHeight.value)
        s"[ Matrix (${m} x ${n} - too large to display) ]"
      else if (m == 1)
        "[" ++ row(ArraySeq.unsafeWrapArray(x.toArray).map(_.show)) ++ "]"
      else {
        import scala.collection.immutable.Vector
        val rows: Vector[Vector[String]] =
          x.toArray.toVector.map(_.show).grouped(n).toVector
        val maxColWidth: Int = {
          for {
            row <- rows
            col <- row
          } yield col.length
        }.max
        val alignedRows: Vector[String] =
          rows map { cols =>
            cols map (s => " " * (maxColWidth - s.length) ++ s) mkString separator.value
          }
        lazy val blankRow
            : String = " " * alignedRows.headOption.map(_.length).getOrElse(0)
        if (Set(UTF_8, UTF_16) contains charset)
          s"┌$blankRow┐" +: alignedRows
            .map("│" ++ _ ++ "│") :+ s"└$blankRow┘"
        else
          alignedRows.map("|" ++ _ ++ "|")
      }.mkString("\n")
    }
}
