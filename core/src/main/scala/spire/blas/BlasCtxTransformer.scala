package spire.blas

import spire.algebra.VectorSpace
import spire.blas.Side._
import spire.blas.Transpose._
import spire.blas.BlasCtx
import spire.blas.BlasCtx._

protected trait BlasCtxTransformer[K, M, N] extends Transformer {
  self: VectorSpace[BlasCtx.Expr[K, M, N], K] =>
  type Term = BlasCtx.Expr[K, M, N]

  type From = Matrix.Expr[K, M, N]

  private lazy val z: Matrix.Expr[K, M, N] =
    (_, _, _) => throw new Error("BlasCtxTransformer: Term in B position isOne")

  def pure(x: From): Term = {
    case Unknown => _ => x
    case _ =>
      f => f(left, -1, scalar.one, x, noTrans, false, z, noTrans, true)
  }

  def eval(t: Term): From = {
    t(Unknown)(
      (_, _, _, _, _, _, _, _, _) =>
        throw new Error("BlasCtxTransformer: Context unknown")
    )
  }
}
