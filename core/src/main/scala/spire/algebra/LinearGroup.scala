package spire.algebra

import spire.algebra.Group

trait LinearGroup[F[_]] extends LinearMap[λ[(m, n) => F[m]]] { self =>

  def inverse[M: ValueOf: ? <:< Int](f: F[M]): F[M]

  def algebra[M: ValueOf: ? <:< Int]: Group[F[M]] = new Group[F[M]] {

    def inverse(a: F[M]): F[M] = self inverse a

    def empty: F[M] = self.id[M]

    def combine(x: F[M], y: F[M]): F[M] = self mmult (x, y)
  }
}

object LinearGroup { //written by hand as the lambda type above breaks @typeclass
  def apply[F[_]](implicit instance: LinearGroup[F]): LinearGroup[F] = instance

  trait Ops[F[_], M] {
    def typeClassInstance: LinearGroup[F]
    def self: F[M]
    def i(implicit M: ValueOf[M], ev: M <:< Int): F[M] =
      typeClassInstance inverse self
  }

  trait ToLinearGroupOps {
    implicit def toLinearGroupOps[F[_], M](
        target: F[M]
    )(implicit tc: LinearGroup[F]): Ops[F, M] = new Ops[F, M] {
      val self = target
      val typeClassInstance = tc
    }
  }

  object nonInheritedOps extends ToLinearGroupOps

  trait AllOps[F[_], M] extends Ops[F, M] {
    def typeClassInstance: LinearGroup[F]
  }

  object ops {
    implicit def toAllLinearGroupOps[F[_], M](
        target: F[M]
    )(implicit tc: LinearGroup[F]): AllOps[F, M] = new AllOps[F, M] {
      val self = target
      val typeClassInstance = tc
    }
  }
}
