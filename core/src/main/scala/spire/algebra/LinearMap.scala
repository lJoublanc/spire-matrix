package spire.algebra

import simulacrum._

/** A linear map.
  * This is used to generalize matrix operations.
  * This is isomorphic to `cats.Category`, but we need to provide evidence that
  * `M` and `N` are integral.
  */
@typeclass trait LinearMap[F[_, _]] {

  /** Matrix multiplication. */
  @op("*") def mmult[
      M: ValueOf: ? <:< Int,
      T: ValueOf: ? <:< Int,
      N: ValueOf: ? <:< Int
  ](f: F[M, T], g: F[T, N]): F[M, N]

  /** Generalization of the transpose of a matrix. */
  @op("t") def transpose[M: ValueOf: ? <:< Int, N: ValueOf: ? <:< Int](
      f: F[M, N]
  ): F[N, M]

  /** Generalization of the matrix conjugate. */
  def adjoint[M: ValueOf: ? <:< Int, N: ValueOf: ? <:< Int](f: F[M, N]): F[M, N]

  /** Generalization of the conjugate transpose of a matrix. */
  @op("h") def hermitian[M: ValueOf: ? <:< Int, N: ValueOf: ? <:< Int](
      f: F[M, N]
  ): F[N, M] = transpose(adjoint(f))

  /** The identity operator. */
  def id[N: ValueOf: ? <:< Int]: F[N, N]
}
