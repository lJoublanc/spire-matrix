package spire.laws

import spire.algebra.{LinearMap, LinearGroup, Field, VectorSpace, Eq}
import spire.syntax.vectorSpace._
import spire.syntax.linearMap._
import spire.syntax.eq._
import spire.blas.DenseBuffer

import org.typelevel.discipline.{Laws, Predicate}

import org.scalacheck.{Arbitrary, Prop}
import org.scalacheck.Prop._

import InvalidTestException._

object LinearLaws {
  def apply[F[_, _], M, N] = new LinearLaws[F, M, N] {}
}

trait LinearLaws[F[_, _], M, N] extends Laws {

  //def vectorSpaceLaws[M,N,K] = VectorSpaceLaws[F[M,N],K]

  /** Laws for a linear map, taken from
    * [[https://en.wikipedia.org/wiki/Linear_map wikipedia]].
    */
  def linearMap[K](
      implicit
      K: Field[K],
      F: LinearMap[F],
      M: ValueOf[M],
      N: ValueOf[N],
      MisInt: M <:< Int,
      NisInt: N <:< Int,
      VMN: VectorSpace[F[M, N], K],
      VNM: VectorSpace[F[N, M], K],
      VM: VectorSpace[F[M, 1], K],
      VN: VectorSpace[F[N, 1], K],
      eqMN: Eq[F[M, N]],
      eqNM: Eq[F[N, M]],
      eqMM: Eq[F[M, M]],
      eqM: Eq[F[M, 1]],
      eqN: Eq[F[N, 1]],
      arbK: Arbitrary[K],
      arbMN: Arbitrary[F[M, N]],
      arbNM: Arbitrary[F[N, M]],
      arbM: Arbitrary[F[M, 1]],
      arbN: Arbitrary[F[N, 1]]
  ) = new RuleSet {
    val name = "linear map"
    def bases = Seq()
    def parents = Seq()
    val props: Seq[(String, Prop)] = Seq(
      "additivity" -> forAllSafe { (f: F[M, N], u: F[N, 1], v: F[N, 1]) =>
        f * (u + v) === (f * u + f * v)
      },
      "homogeneity" -> forAllSafe { (f: F[M, N], c: K, u: F[N, 1]) =>
        (f * (c *: u)) === (c *: (f * u))
      },
      //Transpose laws
      "transpose identity" -> forAllSafe { (f: F[M, N]) =>
        (f.t).t === f
      },
      "transpose distributes over addition" -> forAllSafe {
        (f: F[M, N], g: F[M, N]) =>
          (f + g).t === (f.t + g.t)
      },
      "transpose reverse distributes over mult" -> forAllSafe {
        (f: F[M, N], g: F[N, M]) =>
          (f * g).t === (g.t * f.t)
      },
      "transpose scalar invariance" -> forAllSafe { (f: F[M, N], a: K) =>
        (a *: f.t) === ((a *: f).t)
      }
      /*
      "determinant invariance" -> ???,
      "dot product consistency" -> ???,
      "product is positive-semidefinite" -> ???,
      "commutes with inverse" -> ???,
      "eigenvalue invariance (square matrices)" -> ???
     */
    )
  }
}
