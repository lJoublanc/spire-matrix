package spire.laws

import org.typelevel.discipline.scalatest.Discipline
import org.scalatest.FunSuite
import org.scalacheck.{Arbitrary, Gen}
import org.scalacheck.Arbitrary.arbitrary
import org.scalacheck.util.Pretty
import org.typelevel.discipline.Laws
import org.typelevel.discipline.scalatest._

import spire.algebra.{Eq, Field, VectorSpace, LinearMap}
import spire.optional.ULPOrderDouble
import spire.blas._
import spire.blas.Layout._
import spire.blas.implicits._
import spire.blas.implicits.optimizations._
import spire.blas.BlasCtx
import spire.blas.BlasCtx.Expr

import java.nio
import scala.reflect.ClassTag

/* These tests need to be run multiple times to check the different code-paths. */
class DenseDoubleArrayToArrayAlgebraLaws
    extends BlasAlgebraLaws[Double, nio.DoubleBuffer]
    with ULPEquality
    with FromArray[Double, nio.DoubleBuffer]
    with ToArray[Double, nio.DoubleBuffer]

abstract class BlasAlgebraLaws[K, B](
    implicit
    val B: DenseBuffer.Aux[K, B],
    val C: CBlas.Aux[K, B],
    val ct: ClassTag[K]
) extends FunSuite
    with Discipline {

  implicit def fieldK: Field[K]

  implicit def eqK: Eq[K]

  implicit def arbK: Arbitrary[K]

  implicit def matrixEquality[M: ValueOf: ? <:< Int, N: ValueOf: ? <:< Int]
      : Eq[Expr[K, M, N]]

  implicit def tensorArb[M: ValueOf: ? <:< Int, N: ValueOf: ? <:< Int]
      : Arbitrary[Expr[K, M, N]]

  /** This is necessary to print vector in scalatest output. */
  override def checkAll(name: String, ruleSet: Laws#RuleSet): Unit =
    for ((id, prop) <- ruleSet.all.properties) {
      val p = prop map { res =>
        res.copy(args = res.args.map { a =>
          lazy val z = null.asInstanceOf[B]
          if (a.arg.isInstanceOf[Matrix.Expr[_, _, _]])
            a.copy(
              prettyArg = Pretty { params =>
                {
                  val f = a.arg.asInstanceOf[Matrix.Expr[K, _, _]]
                  val elems = f(z, colMajor, None)
                  "Array(" + Seq
                    .tabulate(B.size(elems))(B.get(elems)(_))
                    .mkString(",") + ")"
                }
              }
            )
          else if (a.arg.isInstanceOf[BlasCtx.Expr[_, _, _]])
            a.copy(
              prettyArg = Pretty {
                params =>
                  {
                    val f = a.arg.asInstanceOf[BlasCtx.Expr[K, _, _]]
                    // format: off
                    val elems = (
                      f(BlasCtx.Unknown)
                      { (_,_,_,_,_,_,_,_,_) => throw new Exception("Dead code") }
                      (z, colMajor,None)
                     )
                    // format: on

                    "Array(" + Seq
                      .tabulate(B.size(elems))(B.get(elems)(_))
                      .mkString(",") + ")"
                  }
              }
            )
          else a
        })
      }
      test(name + "." + id) {
        check(p)
      }
    }

  checkAll(
    "3 x 1",
    VectorSpaceLaws[Expr[K, 3, 1], K]
      .vectorSpace(VectorSpace[Expr[K, 3, 1], K]) // not sure why I have to pass the vec space explicitly but it fails otherwise.
  )

  checkAll(
    "2 x 3",
    LinearLaws[Expr[K, ?, ?], 2, 3].linearMap[K](
      Field[K],
      LinearMap[Expr[K, ?, ?]],
      implicitly[ValueOf[2]],
      implicitly[ValueOf[3]],
      implicitly[2 <:< Int],
      implicitly[3 <:< Int],
      VectorSpace[Expr[K, 2, 3], K],
      VectorSpace[Expr[K, 3, 2], K],
      VectorSpace[Expr[K, 2, 1], K],
      VectorSpace[Expr[K, 3, 1], K],
      matrixEquality[2, 3],
      matrixEquality[3, 2],
      matrixEquality[3, 3],
      matrixEquality[2, 1],
      matrixEquality[3, 1],
      arbK,
      tensorArb[2, 3],
      tensorArb[3, 2],
      tensorArb[2, 1],
      tensorArb[3, 1]
    )
  )

  checkAll(
    "2 x 2",
    VectorSpaceLaws[Expr[K, 2, 2], K]
      .vectorSpace(VectorSpace[Expr[K, 2, 2], K])
  )

}

trait ULPEquality extends ArbitrarySmallDouble {
  self: BlasAlgebraLaws[Double, nio.DoubleBuffer] =>
  // Override [in]equality operators
  // These two allow us to do 'soft' comparison of floating point numbers.
  object DoubleAlgebra extends spire.std.DoubleAlgebra with ULPOrderDouble {
    val δ = 1000
  }

  override implicit lazy val eqK = DoubleAlgebra
  override implicit lazy val fieldK = DoubleAlgebra
  override implicit lazy val arbK: Arbitrary[Double] =
    arbitrarySmallDouble(4, 1L << 8)
}

protected trait ToArray[K, B] {
  self: BlasAlgebraLaws[K, B] =>

  override implicit def matrixEquality[
      M: ValueOf: ? <:< Int,
      N: ValueOf: ? <:< Int
  ]: Eq[Expr[K, M, N]] =
    (x: Expr[K, M, N], y: Expr[K, M, N]) => {
      //import spire.syntax.eq._ //FIXME: eqK isn't being picked up unless we call it explicitly.
      val xa: Array[K] = x.toArray
      val ya: Array[K] = y.toArray
      var i = 0
      var ok = true
      while (i < xa.length && ok) if (eqK.eqv(xa(i), ya(i))) //FIXME this will fail for stride matrices.
        i += 1 //toBuffer(x, stride = 1)
      else ok = false
      ok
    }
}

protected trait FromArray[K, B] {
  self: BlasAlgebraLaws[K, B] =>
  override implicit def tensorArb[
      M: ValueOf: ? <:< Int,
      N: ValueOf: ? <:< Int
  ]: Arbitrary[Expr[K, M, N]] =
    Arbitrary {
      Gen.containerOfN[Array, K](valueOf[M] * valueOf[N], arbitrary[K]) map {
        as =>
          Vector[M].fromArray(as, colMajor) // FIXME workaround for #13
      }
    }
}
