package spire.laws

import org.scalacheck.Arbitrary
import org.scalacheck.Gen._

trait ArbitrarySmallDouble {

  /** A `scalacheck.Arbitrary` that returns IEEE-754 doubles, constraining
    * the mantissa and fraction.
    *
    * This is used for testing double equality; operation precision will
    * be bounded.
    * @param maxFraction The maximum exponent that will be used.
    * @param maxExponent The maximum fraction that will be used.
    */
  def arbitrarySmallDouble(
      maxExponent: Int,
      maxFraction: Long
  ): Arbitrary[Double] =
    Arbitrary {
      for {
        frac <- choose(-maxFraction, maxFraction)
        exp <- choose(0, maxExponent)
      } yield BigDecimal(frac, exp).toDouble
    } ensuring {
      (maxExponent > 0) &&
      (maxExponent < (2 << 11)) &&
      (maxFraction > 0L) &&
      (maxFraction < (2L << 52))
    }
}
