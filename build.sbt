name := "spire-matrix"

lazy val spireVersion = "0.16.1"

enablePlugins(GitVersioning)

lazy val commonSettings = Seq(
  organization := "com.joublanc",
  scalaVersion := "2.13.0-M5",
  libraryDependencies ++= Seq(
    "org.typelevel" %% "spire" % spireVersion,
    "com.github.mpilquist" %% "simulacrum" % "0.14.0",
    "org.typelevel" %% "spire-laws" % spireVersion % "test",
    "org.scalatest" %% "scalatest" % "3.0.6" % "test"
  ), //3.0.7 incompat with discipline/laws
  scalacOptions ++= Seq(
    "-language:higherKinds",
    "-language:reflectiveCalls",
    "-language:implicitConversions",
    "-feature",
    "-Ywarn-unused:imports",
    "-Xlint",
    "-Ymacro-annotations",
    "-deprecation"
  ),
  scalacOptions in (Compile, console) ~=
    (_ diff Seq("-Ywarn-unused:imports", "-Xlint")),
  scalacOptions in (Test, console) ~=
    (_ diff Seq("-Ywarn-unused:imports", "-Xlint")),
  addCompilerPlugin("org.spire-math" %% "kind-projector" % "0.9.9"),
  initialCommands in console += """
    import spire.std.double._
    import spire.algebra.VectorSpace
    import spire.syntax.vectorSpace._
    import spire.algebra.{LinearMap,LinearGroup}
  """,
  scalafmtOnCompile := true,
  developers := List(
    Developer(
      "lJoublanc",
      "Luciano Joublanc",
      "luciano@joublanc.com",
      url("https://gitlab.com/ljoublanc")
    )
  )
)

lazy val noPublish = Seq(
  publish := {},
  publishLocal := {},
  //publishSigned := {},
  publishArtifact := false
)

lazy val root = project
  .in(file("."))
  .settings(commonSettings)
  .settings(noPublish)
  .aggregate(core, laws, benchmark)

lazy val core = (project in file("core"))
  .settings(commonSettings)
  .settings(
    name := "spire-matrix-core",
    description := "Matrices for Spire (BLAS implementation)",
    libraryDependencies ++= Seq(
      "org.typelevel" %% "cats-core" % "1.6.0",
      "net.java.dev.jna" % "jna" % "5.2.0",
      "org.typelevel" %% "cats-effect" % "1.2.0" % "test",
      "org.scalatest" %% "scalatest" % "3.0.6" % "test"
    ),
    initialCommands in console += """
      import spire.blas._
      import spire.blas.implicits._
      import cats.instances.blas._
      import cats.instances.double.catsStdShowForDouble
      import spire.syntax.linearMap._
      import spire.syntax.linearGroup._
      import cats.syntax.show._
      import scala.language.postfixOps
    """,
    initialCommands in consoleQuick := """
      import spire.std.double._
      import spire.algebra.VectorSpace
      import spire.syntax.vectorSpace._
    """,
    initialCommands in (Test, console) += """
      import java.nio.DoubleBuffer
      import spire.blas.CompileSpec
      import cats.effect.SyncIO
      import scala.collection.immutable.ArraySeq
    """,
    fork in Test := true, // Failed malloc/free in BLAS will terminate
    sources in (Compile, doc) := Nil // Bug https://github.com/scala/bug/issues/11045
  )

lazy val laws = (project in file("laws"))
  .settings(commonSettings)
  .settings(noPublish)
  .settings(
    name := "spire-matrix-laws",
    description := "Laws & Tests for matrices",
    libraryDependencies ++= Seq(
      "org.typelevel" %% "spire-laws" % spireVersion
    )
  )
  .dependsOn(core)

lazy val benchmark = (project in file("benchmark"))
  .settings(commonSettings)
  .settings(noPublish)
  .settings(
    name := "spire-matrix-benchmark",
    description := "Benchmarks for Spire-Matrix"
  )
  .enablePlugins(JmhPlugin)
  .dependsOn(core)
