# Spire Matrix

An add-on to [spire](https://github.com/non/spire) for matrices and linear algebra.

Copyright © 2019 Luciano Joublanc

## Features

The project strives to provide a performant API, while having the following distinguishing features:

* *Functionally pure API* which guarantees referential transparency. Categorical abstractions such as `cats.Comonad` are favoured over indexed `(i,j)` accessors.

* *Typed tensor dimensions*. For instance `Matrix[2,3]` or `Vector[5]`. 
  - Code completion is context-sensitive e.g. no `invert` on a non-square matrix.
  - Dimension mismatches caught at compile time.

* *Low GC overhead*. The library aims to be suitable for low-latency applications. Tagless-final in combination with CPS and Java 8 lambdas aim to provide O(1) heap allocations.

* *BLAS compliance*. Rich set of features from the BLAS reference specification are covered:
  - Both real (double and single precision) and Complex (double precision only) algebras.
  - Dense layout: both row/col major, strides, and triangular packed/unpacked matrices.
  - Optimizing interpreter that generates an efficient sequence of BLAS calls, approaching performance of hand crafted code.

## API

Currently due to a bug API generation is disabled; should be fixed in 2.13 final.

## Installation

Add the following to your `build.sbt` file.

```scala mdoc
libraryDependencies += "com.joublanc" % "spire-matrix" % "@VERSION@"
```

## External Dependencies

* Scala 2.13 (should also work with typelevel 2.12.3 Scala compiler) due to [type-literal support](https://github.com/typelevel/scala/blob/typelevel-readme/notes/typelevel-4.md#literal-types-pull5310-milesabin).
* Native BLAS system libraries. Most desktop systems ship with a default implementation of these. Refer to your distribution documentation for how to configure these.

## Related Work

See also:
* [Scala NLP Breeze](https://github.com/scalanlp/breeze/) Used by many machine learning libraries.
* [Luc J Bourhis' Spire Fork](https://github.com/luc-j-bourhis/spire/tree/topic/matrix-wip) An alternative implementation also backed by BLAS. Last updated 2013.
* [Olivier Blanvillain's blog](https://gist.github.com/OlivierBlanvillain/48bb5c66dbb0557da50465809564ee80) has an exposition of CPS with tagless final, which inspired the current implementation.

Netlib:
Most of the numerical routines are implemented via calls to Netlib's soubroutines in 
* [LAPACK](http://www.netlib.org/lapack/)
* [BLAS](http://www.netlib.org/blas/) which is distributed with LAPACK.
* [netlib-java](https://gitlab.com/fommil/attic/tree/master/netlib-java) Java bindings to netlib, which unfortunately appears to have been [discontinued](https://stackoverflow.com/questions/46267411/has-netlib-java-been-discontinued). Note this is no longer a dependency. Matrix' uses JNA to call CBLAS (C rather than Fortran bindings for BLAS) instead.

